# Linux Camera Tricks
This is a collection of a few shell scripts that do weird camera effects using
v4l2 on linux. Add them to your PATH and have fun. `camtricks` is a dmenu
script that gives you access to all of them.
